package com.rytan.moviecollection;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EFragment;
import com.googlecode.androidannotations.annotations.ViewById;
import com.rytan.moviecollection.tmdb.TMDb;
import com.rytan.moviecollection.tmdb.movie.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Nate on 5/23/13.
 */
@EFragment(R.layout.addcollection)
public class MovieSearchFragment extends Fragment {
    TMDb tmdb = new TMDb(Constants.apiKey);
    String title;
    @ViewById(R.id.movie_title_input)
    EditText mTitle;

    int page;

    String language;
    @ViewById(R.id.movie_language_input)
    Spinner mLanguage;

    Boolean adult = false;

    String year;
    @ViewById(R.id.movie_year_input)
    EditText mYear;

    ArrayList<Movie> movies = new ArrayList<Movie>();

    @ViewById(R.id.movies_list)
    ListView mListView;

    private ProgressDialog pDialog;

    private final CustomAdapter mAdapter = new CustomAdapter();

    private FragmentActivity fa;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //TODO: get data from intent in stead of static variable
        //Bundle extras = getActivity().getIntent().getExtras();
        //movieID = extras.getInt("movie");

        fa = super.getActivity();
        View view = inflater.inflate(R.layout.addcollection,
                container, false);
        mListView = (ListView) view.findViewById(R.id.movies_list);

        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(fa, SearchView_.class);
                Movie movie = movies.get(position);
                int movieID = (Integer) movie.getId();
                i.putExtra("movie", movieID);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

        return view;
    }

    @AfterViews
    void After() {

    }

    @Click(R.id.search_database)
    void BtnSearchDatabase() {
        movies.clear();

        title = mTitle.getText().toString();
        year = mYear.getText().toString();
        String tempLanguage = mLanguage.getSelectedItem().toString();
        language = tempLanguage.substring(tempLanguage.length() - 2);

        pDialog = ProgressDialog.show(this.getActivity(), "Retrieving Data", "please wait...", true, false);
        movies.clear();

        LoadList myTask = new LoadList();
        myTask.execute();
        mTitle.clearFocus();
    }


    private class LoadList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {

            pDialog.show();
        }

        //TODO: optimize this to make it work
        @Override
        protected Void doInBackground(Void... params) {

            String result = "";

            if (title != null) {
                if (year.isEmpty()) {
                    try {
                        result = tmdb.SearchMovieByTitle(title, 1, language);

                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    try {
                        result = tmdb.SearchMovieByTitle(title, 1, language, adult, year);
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
                String temp = result.substring(result.indexOf("["));
                result = temp;
                JSONArray tempArray;
                try {
                    tempArray = new JSONArray(result);
                    for (int i = 0; i < tempArray.length(); i++) {
                        JSONObject film = tempArray.getJSONObject(i);
                        Movie example = new Movie();
                        example.setAdult(false);
                        example.setBackdrop_path(film.getString("backdrop_path"));
                        example.setId(Integer.parseInt(film.getString("id")));
                        example.setOriginal_title(film.getString("original_title"));
                        example.setRelease_date(film.getString("release_date"));
                        example.setPoster_path(film.getString("poster_path"));
                        example.setPopularity(Float.parseFloat(film.getString("popularity")));
                        example.setTitle(film.getString("title"));
                        if (!film.getString("vote_average").equals("nan")  ){
                            example.setVote_average(Float.parseFloat(film.getString("vote_average")));
                        }
                        if (film.getString("vote_count") != ""){
                            example.setVote_count(Integer.parseInt(film.getString("vote_count")));
                        }
                        movies.add(example);
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pDialog.dismiss();
            mListView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

            title = "";
            year = "";
            language = "";
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            bmImage.setImageBitmap(result);
            //close
            pDialog.dismiss();
        }
    }

    private class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return movies.size();
        }

        @Override
        public Movie getItem(final int position) {
            return movies.get(position);
        }

        @Override
        public long getItemId(final int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            //TODO: Show progress dialog while loading view for list

            View v = convertView;
            if (v == null) {
                v = getActivity().getLayoutInflater().inflate(R.layout.triplerow, parent, false);
            }

            Movie movie = movies.get(position);
            title = movie.getTitle();

            if (movie.getRelease_date() != null) {
                year = movie.getRelease_date();
            } else {
                year = "Unknown";
            }

            TextView titleText = (TextView) v.findViewById(R.id.triplerow_first);
            TextView yearText = (TextView) v.findViewById(R.id.triplerow_second);
            TextView ratingText = (TextView) v.findViewById(R.id.triplerow_third);
            ImageView posterImage = (ImageView) v.findViewById(R.id.movie_poster);
            String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w92" + movie.getPoster_path().toString();
            new DownloadImageTask(posterImage).execute(path);

            titleText.setText(title);
            yearText.setText(year);
            ratingText.setText(movie.getVote_average().toString());

            return v;
        }

    }
}
