package com.rytan.moviecollection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.db4o.ObjectSet;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EFragment;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.ViewById;
import com.rytan.moviecollection.Database.DatabaseHelper;
import com.rytan.moviecollection.ImageCache;
import com.rytan.moviecollection.tmdb.TMDb;
import com.rytan.moviecollection.tmdb.movie.Genre;
import com.rytan.moviecollection.tmdb.movie.Movie;
import com.rytan.moviecollection.tmdb.movie.ProductionCompany;
import com.rytan.moviecollection.tmdb.movie.ProductionCountry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import static android.os.Environment.isExternalStorageRemovable;
import static com.rytan.moviecollection.ImageCache.getExternalCacheDir;

/**
 * User: Nate
 * Date: 5/13/13
 */
@EFragment(R.layout.cast_fragment)
public class CollectionDetailFragment extends Fragment {
    TMDb tmdb = new TMDb(Constants.apiKey);
    private BlobStorage mBlobStorage;

    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";

    ArrayList<Movie> databaseResult;
    Movie movie;

    int movieID;
    NumberFormat formatter = NumberFormat.getCurrencyInstance();

    private DatabaseHelper dbHelper = null;
    String database;

    @ViewById(R.id.button2)
    TextView mWishlist;

    @ViewById(R.id.mainDetailsLinearLayout)
    LinearLayout mainLinearLayout;

    public static final CollectionDetailFragment newInstance(String message) {

        CollectionDetailFragment_ f = new CollectionDetailFragment_();

        Bundle bdl = new Bundle(1);
        bdl.putString(EXTRA_MESSAGE, message);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //TODO: get data from intent in stead of static variable
        Bundle extras = getActivity().getIntent().getExtras();
        movieID = extras.getInt("movie");
        database = getResources().getString(R.string.collection_database);
        dbHelper();
        // pDialog = ProgressDialog.show(getActivity(), "Retrieving Data", "please wait...", true, false);


        LoadMovie loadMovie = new LoadMovie();
        loadMovie.execute();
        View view = inflater.inflate(R.layout.movie_detail,
                container, false);

        return view;
    }


    @Override
    public void onPause() {
        super.onDestroy();
        dbHelper().close();
        dbHelper = null;
    }

    private DatabaseHelper dbHelper() {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(getActivity());
            dbHelper.db(database);
        }
        return dbHelper;
    }

    @AfterViews
    void After() {

    }


    @Click(R.id.remove_movie)
    void BtnRemove() {
        Movie search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null,null,null,null,null);
        try {
            mBlobStorage = dbHelper.db(database).query(BlobStorage.class).get(0);
            ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);
            while (result.hasNext()) {
                movie = result.next();
            }
            dbHelper.db(database).delete(movie);

        } finally {
            dbHelper.db(database).close();
        }
    }


    private class LoadMovie extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            Movie search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                    , null, null, null, null, null, null, null, null, null, null,null,null,null,null);
            try {
                ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);

                while (result.hasNext()) {

                    movie = result.next();
                }

            } finally {
                dbHelper.db(database).close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            PostExecute();

        }
    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected void onPreExecute() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(),mIcon11);

            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            try {
                bmImage.setImageBitmap(result);
                //close

            } catch (Exception ex) {
                Log.e("ERROR", "can't assign image to view");
            }

        }
    }

    private List<Genre> getGenreAsList(JSONArray genres) {
        ArrayList<Genre> listGenres = new ArrayList<Genre>();
        if (genres != null) {
            for (int x = 0; x < genres.length(); x++) {
                try {
                    JSONObject temp = genres.getJSONObject(x);
                    Genre genre = new Genre();
                    genre.setId(Integer.parseInt(temp.getString("id")));
                    genre.setName(temp.getString("name"));
                    listGenres.add(genre);
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return listGenres;
    }

    private List<ProductionCompany> getProductionCompaniesAsList(JSONArray genres) {
        ArrayList<ProductionCompany> listCompanies = new ArrayList<ProductionCompany>();
        if (genres != null) {
            for (int x = 0; x < genres.length(); x++) {
                try {
                    JSONObject temp = genres.getJSONObject(x);
                    ProductionCompany company = new ProductionCompany();
                    company.setId(Integer.parseInt(temp.getString("id")));
                    company.setName(temp.getString("name"));
                    listCompanies.add(company);
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return listCompanies;
    }

    private List<ProductionCountry> getProductionCountriesAsList(JSONArray genres) {
        ArrayList<ProductionCountry> listCompanies = new ArrayList<ProductionCountry>();
        if (genres != null) {
            for (int x = 0; x < genres.length(); x++) {
                try {
                    JSONObject temp = genres.getJSONObject(x);
                    ProductionCountry country = new ProductionCountry();
                    country.setIso_3166_1(temp.getString("iso_3166_1"));
                    country.setName(temp.getString("name"));
                    listCompanies.add(country);
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return listCompanies;
    }


    @UiThread
    void PostExecute() {
        //TODO: Finish adding attributes to this view(WHERE THE HELL SHOULD THE CODE BE PUT?)
        if (movie.getBackdrop_path() != null) {
            ImageView backdrop = (ImageView) mainLinearLayout.findViewById(R.id.movieBackDrop);
            movie.setBackdrop(movie.getBitmapFromBytes(movie.getBackdropBytes()));
            if (movie.getPoster() == null) {
                String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/original" + movie.getBackdrop_path();
                new DownloadImageTask(backdrop).execute(path);
            } else {
                backdrop.setImageBitmap(movie.getBackdrop());
            }
        }

        if (movie.getPoster_path() != null) {
            ImageView poster = (ImageView) mainLinearLayout.findViewById(R.id.movie_posterview);
            movie.setPoster(movie.getBitmapFromBytes(movie.getPosterBytes()));
            if (movie.getPoster() == null) {
                String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/original" + movie.getPoster_path();
                new DownloadImageTask(poster).execute(path);
            } else {
                poster.setImageBitmap(movie.getPoster());
            }
        }
        TextView temp = (TextView) mainLinearLayout.findViewById(R.id.movieOriginalTitle);
        temp.setText(movie.getOriginal_title());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieGenre);
        temp.setText(movie.getGenres());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieStatus);
        temp.setText(movie.getStatus());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieReleaseDate);
        temp.setText(movie.getRelease_date());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieTagline);
        temp.setText(movie.getTagline());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieDescription);
        temp.setText(movie.getOverview());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieProductionCompanies);
        temp.setText(movie.getProduction_companies());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieProductionCountries);
        temp.setText(movie.getProduction_countries());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieBudget);
        temp.setText(String.valueOf(formatter.format(movie.getBudget())));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieRevenue);
        temp.setText(String.valueOf(formatter.format(movie.getRevenue())));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieRuntime);
        temp.setText(String.valueOf(movie.getRuntime()) + " Minutes");

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieHomepage);
        temp.setText(movie.getHomepage());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieVoteCount);
        temp.setText(String.valueOf(movie.getVote_count()));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieVoteAverage);
        temp.setText(String.valueOf(movie.getVote_average()));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieTitle);
        temp.setText(movie.getTitle());

        mainLinearLayout.invalidate();

    }

}