package com.rytan.moviecollection.Database;


import android.content.Context;
import com.rytan.moviecollection.tmdb.movie.Movie;

import java.util.List;

/**
 * User: Nate
 * Date: 5/8/13
 */
public class MovieProvider extends DatabaseHelper {

    public final static String TAG = "MovieProvider";
    private static MovieProvider provider = null;

    public MovieProvider(Context ctx) {
        super(ctx);
    }

    public static MovieProvider getInstance(Context ctx) {
        if (provider == null)
            provider = new MovieProvider(ctx);
        return provider;
    }

    public void store(Movie movie, String database) {
        db(database).store(movie);
    }

    public void delete(Movie movie, String database) {
        db(database).delete(movie);
    }

    public List findAll(String database) {
        return db(database).query(Movie.class);
    }

}
