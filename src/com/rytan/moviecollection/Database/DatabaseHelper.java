package com.rytan.moviecollection.Database;

import android.content.Context;
import android.util.Log;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.config.EmbeddedConfiguration;
import com.rytan.moviecollection.tmdb.movie.Movie;

import java.io.IOException;

/**
 * User: Nate
 * Date: 5/8/13
 */
public class DatabaseHelper {

    private static ObjectContainer oc = null;
    private Context context;

    public DatabaseHelper(Context ctx) {
        context = ctx;
    }

    public ObjectContainer db(String database) {
        try {
            if (oc == null || oc.ext().isClosed()) {
                oc = Db4oEmbedded.openFile(dbConfig(), db4oDBFullPath(context, database));
            }

            return oc;
        } catch (Exception ie) {
            Log.e(DatabaseHelper.class.getName(), ie.toString());
            return null;
        }
    }

    /**
     * Configure the behavior of the database
     */

    private EmbeddedConfiguration dbConfig() throws IOException {
        EmbeddedConfiguration configuration = Db4oEmbedded.newConfiguration();
        configuration.common().objectClass(Movie.class).objectField("title").indexed(true);
        configuration.common().objectClass(Movie.class).objectField("id").indexed(true);
        configuration.common().objectClass(Movie.class).cascadeOnUpdate(true);
        configuration.common().objectClass(Movie.class).cascadeOnActivate(true);
        return configuration;
    }

    /**
     * Returns the path for the database location
     */

    private String db4oDBFullPath(Context ctx, String database) {
        return ctx.getDir("data", 0) + "/" + database;
    }

    /**
     * Closes the database
     */

    public void close() {
        if (oc != null)
            oc.close();
    }

}

