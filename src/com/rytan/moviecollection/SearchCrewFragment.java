package com.rytan.moviecollection;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.db4o.ObjectSet;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EFragment;
import com.rytan.moviecollection.Database.DatabaseHelper;
import com.rytan.moviecollection.tmdb.TMDb;
import com.rytan.moviecollection.tmdb.collections.Cast;
import com.rytan.moviecollection.tmdb.collections.Crew;
import com.rytan.moviecollection.tmdb.collections.MovieCast;
import com.rytan.moviecollection.tmdb.movie.Genre;
import com.rytan.moviecollection.tmdb.movie.Movie;
import com.rytan.moviecollection.tmdb.movie.ProductionCompany;
import com.rytan.moviecollection.tmdb.movie.ProductionCountry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Nate on 5/28/13.
 */
@EFragment(R.layout.cast_fragment)
public class SearchCrewFragment extends ListFragment {
    private ProgressDialog pDialog;
    TMDb tmdb = new TMDb(Constants.apiKey);


    ArrayList<Movie> databaseResult;

    Movie movie;
    Cast temp1;
    Crew temp2;

    int movieID = 0;
    NumberFormat formatter = NumberFormat.getCurrencyInstance();

    private DatabaseHelper dbHelper = null;
    String database;


    private final CustomAdapter mAdapter = new CustomAdapter();

    private FragmentActivity fa;

    ArrayList<Crew> mCrew;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle extras = getActivity().getIntent().getExtras();
        movieID = extras.getInt("movie");
        // pDialog = ProgressDialog.show(getActivity(), "Retrieving Data", "please wait...", true, false);

        fa = super.getActivity();

        View view = inflater.inflate(R.layout.cast_fragment,
                container, false);

        setListAdapter(mAdapter);

        LoadMovie load = new LoadMovie();
        load.execute();

        databaseResult = new ArrayList<Movie>();

        mCrew = new ArrayList<Crew>();

        LoadList crew = new LoadList();
        crew.execute();

        return view;
    }

    @AfterViews
    void After() {

    }

    private DatabaseHelper dbHelper() {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(getActivity());
            dbHelper.db(database);
        }
        return dbHelper;
    }


    @Click(R.id.add_to_collection)
    void BtnCollection() {
        Movie search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null);
        ;
        database = getResources().getString(R.string.collection_database);
        dbHelper();
        try {
            ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);
            while (result.hasNext()) {
                databaseResult.add(result.next());
            }

            if (databaseResult.size() < 1) {

                dbHelper.db(database).store(movie);
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie added to Collection", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie already in Collection", Toast.LENGTH_LONG).show();
            }

        } finally {
            dbHelper.db(database).close();
        }
        databaseResult = null;
    }

    @Click(R.id.add_to_wishlist)
    void BtnWishList() {
        Movie search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null);
        database = getResources().getString(R.string.wishlist_database);
        dbHelper();
        try {
            ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);
            while (result.hasNext()) {
                databaseResult.add(result.next());
            }

            if (databaseResult.size() < 1) {

                dbHelper.db(database).store(movie);
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie added to Wishlist", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie already in Wishlist", Toast.LENGTH_LONG).show();
            }

        } finally {
            dbHelper.db(database).close();
        }
        databaseResult = null;
    }


    private class LoadList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {

            //pDialog.show();
        }

        //TODO: optimize this to make it work
        @Override
        protected Void doInBackground(Void... params) {

            String result = "";


            try {
                result = tmdb.GetMovieCast(Integer.toString(movieID));

            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            JSONObject tempObj;
            JSONArray tempArray;

            try {
                tempObj = new JSONObject(result);
                tempArray = tempObj.getJSONArray("crew");
                for (int i = 0; i < tempArray.length(); i++) {
                    JSONObject actor = tempArray.getJSONObject(i);
                    Crew example = new Crew();
                    example.setId(Integer.valueOf(actor.getString("id")));
                    example.setName(actor.getString("name"));
                    example.setJob(actor.getString("job"));
                    example.setDepartment(actor.getString("department"));
                    example.setProfile_path(actor.getString("profile_path"));
                    mCrew.add(example);
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //pDialog.dismiss();
            setListAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

        }
    }

    private List<Genre> getGenreAsList(JSONArray genres) {
        ArrayList<Genre> listGenres = new ArrayList<Genre>();
        if (genres != null) {
            for (int x = 0; x < genres.length(); x++) {
                try {
                    JSONObject temp = genres.getJSONObject(x);
                    Genre genre = new Genre();
                    genre.setId(Integer.parseInt(temp.getString("id")));
                    genre.setName(temp.getString("name"));
                    listGenres.add(genre);
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return listGenres;
    }

    private List<ProductionCompany> getProductionCompaniesAsList(JSONArray genres) {
        ArrayList<ProductionCompany> listCompanies = new ArrayList<ProductionCompany>();
        if (genres != null) {
            for (int x = 0; x < genres.length(); x++) {
                try {
                    JSONObject temp = genres.getJSONObject(x);
                    ProductionCompany company = new ProductionCompany();
                    company.setId(Integer.parseInt(temp.getString("id")));
                    company.setName(temp.getString("name"));
                    listCompanies.add(company);
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return listCompanies;
    }

    private List<ProductionCountry> getProductionCountriesAsList(JSONArray genres) {
        ArrayList<ProductionCountry> listCompanies = new ArrayList<ProductionCountry>();
        if (genres != null) {
            for (int x = 0; x < genres.length(); x++) {
                try {
                    JSONObject temp = genres.getJSONObject(x);
                    ProductionCountry country = new ProductionCountry();
                    country.setIso_3166_1(temp.getString("iso_3166_1"));
                    country.setName(temp.getString("name"));
                    listCompanies.add(country);
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return listCompanies;
    }


    private class LoadMovie extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {

        }


        @Override
        protected Void doInBackground(Void... params) {

            String result = "";

            try {
                result = tmdb.SearchMovieById(String.valueOf(movieID));
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            try {
                JSONObject film = new JSONObject(result);
                Movie example = new Movie();
                example.setAdult(false);
                example.setBackdrop_path(film.getString("backdrop_path"));
                example.setId(Integer.parseInt(film.getString("id")));
                example.setOriginal_title(film.getString("original_title"));
                example.setRelease_date(film.getString("release_date"));
                example.setPoster_path(film.getString("poster_path"));
                example.setGenres(getGenreAsList(film.getJSONArray("genres")));
                example.setPopularity(Float.parseFloat(film.getString("popularity")));
                example.setTitle(film.getString("title"));
                example.setProduction_companies(getProductionCompaniesAsList(film.getJSONArray("production_companies")));
                example.setProduction_countries(getProductionCountriesAsList(film.getJSONArray("production_countries")));
                example.setVote_average(Float.parseFloat(film.getString("vote_average")));
                example.setVote_count(Integer.parseInt(film.getString("vote_count")));
                //example.setBelongs_to_collection(film.get("belongs_to_collection"));
                example.setHomepage(film.getString("homepage"));
                example.setBudget(Long.parseLong(film.getString("budget")));
                example.setImdb_id(film.getString("imdb_id"));
                example.setOverview(film.getString("overview"));
                example.setRevenue(Integer.parseInt(film.getString("revenue")));
                example.setRuntime(Integer.parseInt(film.getString("runtime")));
                example.setStatus(film.getString("status"));
                example.setTagline(film.getString("tagline"));
                example.setCast(retreveCast((Integer) example.getId()));
                ImageView temp = new ImageView(getActivity());
                String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/original" + example.getBackdrop_path();
                new DownloadBackgroundTask(temp).execute(path);
                path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/original" + example.getBackdrop_path();
                new DownloadPosterTask(temp).execute(path);

                movie = example;
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {

        }
    }

    private MovieCast retreveCast(int id) {
        movieID = id;
        MovieCast cast = new MovieCast();
        ArrayList mCast = new ArrayList<Cast>();
        String result = "";
        try {
            result = tmdb.GetMovieCast(Integer.toString(movieID));

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        JSONObject tempObj;
        JSONArray tempArray;

        try {
            tempObj = new JSONObject(result);
            tempArray = tempObj.getJSONArray("cast");
            for (int i = 0; i < tempArray.length(); i++) {
                JSONObject actor = tempArray.getJSONObject(i);
                Cast example = new Cast();
                example.setId(Integer.valueOf(actor.getString("id")));
                example.setName(actor.getString("name"));
                example.setCharacter(actor.getString("character"));
                example.setOrder(Integer.parseInt(actor.getString("order")));
                example.setProfile_path(actor.getString("profile_path"));
                ImageView temp = new ImageView(getActivity());
                String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w92" + example.getProfile_path();
                temp1 = example;
                new DownloadCastImageTask(temp).execute(path);
                temp1 = null;

                mCast.add(example);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        Collections.sort(mCast, new Comparator<Cast>() {
            @Override
            public int compare(Cast left, Cast right) {
                int leftInt = (Integer) left.order;
                int rightInt = (Integer) right.order;
                return leftInt - rightInt;
            }
        });
        cast.setCast(mCast);

        ArrayList mCrew = new ArrayList<Crew>();
        result = "";
        try {
            result = tmdb.GetMovieCast(Integer.toString(movieID));

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        tempObj = null;
        tempArray = null;

        try {
            tempObj = new JSONObject(result);
            tempArray = tempObj.getJSONArray("crew");
            for (int i = 0; i < tempArray.length(); i++) {
                JSONObject actor = tempArray.getJSONObject(i);
                Crew example = new Crew();
                example.setId(Integer.valueOf(actor.getString("id")));
                example.setName(actor.getString("name"));
                example.setJob(actor.getString("job"));
                example.setDepartment(actor.getString("department"));
                example.setProfile_path(actor.getString("profile_path"));
                ImageView temp = new ImageView(getActivity());
                String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w92" + example.getProfile_path();
                temp2 = example;
                new DownloadCrewImageTask(temp).execute(path);
                temp2 = null;
                mCrew.add(example);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        cast.setCrew(mCrew);

        return cast;
    }

    private class DownloadCastImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadCastImageTask(ImageView bmImage) {
            this.bmImage = bmImage;

        }

        protected void onPreExecute() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            try {
                temp1.setProfile(result);
                bmImage.setImageBitmap(result);
                //close

            } catch (Exception ex) {
                Log.e("ERROR", "can't assign image to view");
            }

        }
    }

    private class DownloadCrewImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadCrewImageTask(ImageView bmImage) {
            this.bmImage = bmImage;

        }

        protected void onPreExecute() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            try {
                temp2.setProfile(result);
                bmImage.setImageBitmap(result);
                //close

            } catch (Exception ex) {
                Log.e("ERROR", "can't assign image to view");
            }

        }
    }

    private class DownloadBackgroundTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadBackgroundTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected void onPreExecute() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            try {
                movie.setBackdrop(result);
                bmImage.setImageBitmap(result);
                //close

            } catch (Exception ex) {
                Log.e("ERROR", "can't assign image to view");
            }

        }
    }

    private class DownloadPosterTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadPosterTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected void onPreExecute() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            try {
                movie.setPoster(result);
                bmImage.setImageBitmap(result);
                //close

            } catch (Exception ex) {
                Log.e("ERROR", "can't assign image to view");
            }

        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            bmImage.setImageBitmap(result);
            //close
        }
    }

    private class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mCrew.size();
        }

        @Override
        public Crew getItem(final int position) {
            return mCrew.get(position);
        }

        @Override
        public long getItemId(final int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            //TODO: Show progress dialog while loading view for list

            View v = convertView;
            if (v == null) {
                v = getActivity().getLayoutInflater().inflate(R.layout.triplerow, parent, false);
            }

            Crew actor = mCrew.get(position);

            TextView titleText = (TextView) v.findViewById(R.id.triplerow_first);
            TextView yearText = (TextView) v.findViewById(R.id.triplerow_second);
            ImageView posterImage = (ImageView) v.findViewById(R.id.movie_poster);
            TextView ratingText = (TextView) v.findViewById(R.id.triplerow_third);
            String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w92" + actor.getProfile_path().toString();
            new DownloadImageTask(posterImage).execute(path);

            titleText.setText(actor.getName());
            yearText.setText(actor.getJob());
            ratingText.setText(actor.getDepartment());
            return v;
        }

    }
}
