package com.rytan.moviecollection;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.db4o.ObjectSet;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EFragment;
import com.rytan.moviecollection.Database.DatabaseHelper;
import com.rytan.moviecollection.tmdb.TMDb;
import com.rytan.moviecollection.tmdb.movie.Movie;

import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Nate on 5/28/13.
 */
@EFragment(R.layout.list_fragment)
public class CollectionFragment extends ListFragment {
    private ProgressDialog pDialog;
    TMDb tmdb = new TMDb(Constants.apiKey);


    ArrayList<Movie> databaseResult;

    Movie movie;

    int movieID = 0;
    NumberFormat formatter = NumberFormat.getCurrencyInstance();

    private DatabaseHelper dbHelper = null;
    String database;


    private final CustomAdapter mAdapter = new CustomAdapter();

    private FragmentActivity fa;

    ArrayList<Movie> movies;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // pDialog = ProgressDialog.show(getActivity(), "Retrieving Data", "please wait...", true, false);

        fa = super.getActivity();

        View view = inflater.inflate(R.layout.list_fragment,
                container, false);

        setListAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        database = getResources().getString(R.string.collection_database);
        dbHelper();
        movies = new ArrayList<Movie>();

        LoadList cast = new LoadList();
        cast.execute();

        return view;
    }
    @Override
    public void onResume(){
        super.onResume();
        mAdapter.notifyDataSetChanged();

    }

    @AfterViews
    void After() {
        setListAdapter(mAdapter);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), CollectionView_.class);
                Movie movie = movies.get(position);
                int movieID = (Integer) movie.getId();
                i.putExtra("movie", movieID);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });

    }

    private DatabaseHelper dbHelper() {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(getActivity());
            dbHelper.db(database);
        }
        return dbHelper;
    }

    private class LoadList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {

            //pDialog.show();
        }

        //TODO: optimize this to make it work
        @Override
        protected Void doInBackground(Void... params) {

            try {
                ObjectSet<Movie> result = dbHelper.db(database).queryByExample(Movie.class);

                while (result.hasNext()) {
                    Movie temp = (Movie) result.next();
                    movies.add(temp);
                    // Log.v("TAG", "Name: " + person.name + " Number: " + person.number + " eMail: " + person.email);
                }
            } finally {
                dbHelper.db(database).close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //pDialog.dismiss();
            Collections.sort(movies);
            setListAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            bmImage.setImageBitmap(result);
            //close
        }
    }

    private class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return movies.size();
        }

        @Override
        public Movie getItem(final int position) {
            return movies.get(position);
        }

        @Override
        public long getItemId(final int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            //TODO: Show progress dialog while loading view for list

            View v = convertView;
            if (v == null) {
                v = getActivity().getLayoutInflater().inflate(R.layout.triplerow, parent, false);
            }

            Movie movie = movies.get(position);
            String title = movie.getTitle();
            String year;

            if (movie.getRelease_date() != null) {
                year = movie.getRelease_date();
            } else {
                year = "Unknown";
            }

            TextView titleText = (TextView) v.findViewById(R.id.triplerow_first);
            TextView yearText = (TextView) v.findViewById(R.id.triplerow_second);
            TextView ratingText = (TextView) v.findViewById(R.id.triplerow_third);
            ImageView posterImage = (ImageView) v.findViewById(R.id.movie_poster);
            String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w92" + movie.getPoster_path().toString();
            new DownloadImageTask(posterImage).execute(path);

            titleText.setText(title);
            yearText.setText(year);
            ratingText.setText(movie.getVote_average().toString());

            return v;
        }
    }
}

