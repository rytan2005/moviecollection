package com.rytan.moviecollection;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.db4o.ObjectSet;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EFragment;
import com.rytan.moviecollection.Database.DatabaseHelper;
import com.rytan.moviecollection.tmdb.TMDb;
import com.rytan.moviecollection.tmdb.collections.Crew;
import com.rytan.moviecollection.tmdb.movie.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by Nate on 5/28/13.
 */
@EFragment(R.layout.cast_fragment)
public class CrewFragment  extends ListFragment {
    private ProgressDialog pDialog;
    TMDb tmdb = new TMDb(Constants.apiKey);


    ArrayList<Movie> databaseResult;

    Movie movie;

    int movieID = 0;
    NumberFormat formatter = NumberFormat.getCurrencyInstance();

    private DatabaseHelper dbHelper = null;
    String database;


    private final CustomAdapter mAdapter = new CustomAdapter();

    private FragmentActivity fa;

    ArrayList<Crew> mCrew;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle extras = getActivity().getIntent().getExtras();
        movieID = extras.getInt("movie");
        // pDialog = ProgressDialog.show(getActivity(), "Retrieving Data", "please wait...", true, false);

        fa = super.getActivity();

        View view = inflater.inflate(R.layout.cast_fragment,
                container, false);

        setListAdapter(mAdapter);

        mCrew = new ArrayList<Crew>();

        LoadList Crew = new LoadList();
        Crew.execute();

        return view;
    }

    @AfterViews
    void After() {

    }

    private DatabaseHelper dbHelper() {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(getActivity());
            dbHelper.db(database);
        }
        return dbHelper;
    }


    @Click(R.id.move_to_collection)
    void BtnCollection() {
        Movie search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null,null,null,null,null);
        ;
        database = getResources().getString(R.string.collection_database);
        dbHelper();
        try {
            ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);
            while (result.hasNext()) {
                databaseResult.add(result.next());
            }


            if (databaseResult.size() < 1) {

                dbHelper.db(database).store(movie);
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie added to Collection", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie already in Collection", Toast.LENGTH_LONG).show();
            }

        } finally {
            dbHelper.db(database).close();
        }
        databaseResult = null;
    }

    @Click(R.id.add_to_wishlist)
    void BtnWishList() {
        Movie search =new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null,null,null,null,null);
        database = getResources().getString(R.string.wishlist_database);
        dbHelper();
        try {
            ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);
            while (result.hasNext()) {
                databaseResult.add(result.next());
            }


            if (databaseResult.size() < 1) {

                dbHelper.db(database).store(movie);
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie added to Wishlist", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie already in Wishlist", Toast.LENGTH_LONG).show();
            }

        } finally {
            dbHelper.db(database).close();
        }
        databaseResult = null;
    }


    private class LoadList extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {

            //pDialog.show();
        }

        //TODO: optimize this to make it work
        @Override
        protected Void doInBackground(Void... params) {

            String result = "";


            try {
                result = tmdb.GetMovieCast(Integer.toString(movieID));

            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            JSONObject tempObj;
            JSONArray tempArray;

            try {
                tempObj = new JSONObject(result);
                tempArray = tempObj.getJSONArray("crew");
                for (int i = 0; i < tempArray.length(); i++) {
                    JSONObject actor = tempArray.getJSONObject(i);
                    Crew example = new Crew();
                    example.setId(Integer.valueOf(actor.getString("id")));
                    example.setName(actor.getString("name"));
                    example.setJob(actor.getString("job"));
                    example.setDepartment(actor.getString("department"));
                    example.setProfile_path(actor.getString("profile_path"));
                    mCrew.add(example);
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //pDialog.dismiss();
            setListAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            bmImage.setImageBitmap(result);
            //close
        }
    }

    private class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mCrew.size();
        }

        @Override
        public Crew getItem(final int position) {
            return mCrew.get(position);
        }

        @Override
        public long getItemId(final int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
            //TODO: Show progress dialog while loading view for list

            View v = convertView;
            if (v == null) {
                v = getActivity().getLayoutInflater().inflate(R.layout.triplerow, parent, false);
            }

            Crew actor = mCrew.get(position);

            TextView titleText = (TextView) v.findViewById(R.id.triplerow_first);
            TextView yearText = (TextView) v.findViewById(R.id.triplerow_second);
            ImageView posterImage = (ImageView) v.findViewById(R.id.movie_poster);
            TextView ratingText = (TextView) v.findViewById(R.id.triplerow_third);
            String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w92" + actor.getProfile_path().toString();
            new DownloadImageTask(posterImage).execute(path);

            titleText.setText(actor.getName());
            yearText.setText(actor.getJob());
            ratingText.setText(actor.getDepartment());
            return v;
        }

    }
}
