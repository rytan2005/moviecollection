package com.rytan.moviecollection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.db4o.ObjectSet;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EFragment;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.ViewById;
import com.rytan.moviecollection.Database.DatabaseHelper;
import com.rytan.moviecollection.ImageCache;
import com.rytan.moviecollection.tmdb.TMDb;
import com.rytan.moviecollection.tmdb.movie.Movie;

import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;

import static android.os.Environment.isExternalStorageRemovable;
import static com.rytan.moviecollection.ImageCache.getExternalCacheDir;

/**
 * User: Nate
 * Date: 5/13/13
 */
@EFragment(R.layout.movie_detail_wishlist)
public class WishlistDetailFragment extends Fragment {
    TMDb tmdb = new TMDb(Constants.apiKey);

    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";

    ArrayList<Movie> databaseResult;
    Movie movie;

    int movieID = 11;
    NumberFormat formatter = NumberFormat.getCurrencyInstance();

    private DatabaseHelper dbHelper = null;
    String database;

    @ViewById(R.id.button2)
    TextView mWishlist;

    @ViewById(R.id.mainDetailsLinearLayout)
    LinearLayout mainLinearLayout;

    public static final WishlistDetailFragment newInstance(String message) {

        WishlistDetailFragment f = new WishlistDetailFragment();

        Bundle bdl = new Bundle(1);
        bdl.putString(EXTRA_MESSAGE, message);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //TODO: get data from intent in stead of static variable
        Bundle extras = getActivity().getIntent().getExtras();
        movieID = extras.getInt("movie");
        database = getResources().getString(R.string.wishlist_database);
        dbHelper();


        // pDialog = ProgressDialog.show(getActivity(), "Retrieving Data", "please wait...", true, false);
        View view = inflater.inflate(R.layout.movie_detail_wishlist,
                container, false);

        LoadMovie loadMovie = new LoadMovie();
        loadMovie.execute();


        return view;
    }


    @Override
    public void onPause() {
        super.onDestroy();
        dbHelper().close();
        dbHelper = null;
    }

    private DatabaseHelper dbHelper() {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(getActivity());
            dbHelper.db(database);
        }
        return dbHelper;
    }

    @AfterViews
    void After() {

    }

    @Click(R.id.move_to_collection)
    void BtnMoveToCollection() {
        databaseResult = new ArrayList<Movie>();
        Movie search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null,null,null,null,null);
        database = getResources().getString(R.string.wishlist_database);
        dbHelper();
        try {
            ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);
            while (result.hasNext()) {
                movie = result.next();
            }
            dbHelper.db(database).delete(movie);

        } finally {
            dbHelper.db(database).close();
        }

        search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null,null,null,null,null);

        database = getResources().getString(R.string.collection_database);
        dbHelper();
        try {
            ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);
            while (result.hasNext()) {
                databaseResult.add(result.next());
            }


            if (databaseResult == null ||databaseResult.size() < 1) {

                dbHelper.db(database).store(movie);
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie added to Collection", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie already in Collection", Toast.LENGTH_LONG).show();
            }

        } finally {
            dbHelper.db(database).close();
        }
        databaseResult = null;
    }

    @Click(R.id.remove_movie)
    void BtnRemove() {
        Movie search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null,null,null,null,null);
        try {
            ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);
            while (result.hasNext()) {
                movie = result.next();
            }
            dbHelper.db(database).delete(movie);

        } finally {
            dbHelper.db(database).close();
        }
    }


    private class LoadMovie extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            Movie search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                    , null, null, null, null, null, null, null, null, null, null,null,null,null,null);
            try {
                ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);

                while (result.hasNext()) {
                    movie = result.next();
                }

            } finally {
                dbHelper.db(database).close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            PostExecute();

        }
    }



    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected void onPreExecute() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(),mIcon11);
            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            try {
                bmImage.setImageBitmap(result);
                //close

            } catch (Exception ex) {
                Log.e("ERROR", "can't assign image to view");
            }

        }
    }

    @UiThread
    void PostExecute() {
        //TODO: Finish adding attributes to this view(WHERE THE HELL SHOULD THE CODE BE PUT?)
        if (movie.getBackdrop_path() != null) {
            ImageView backdrop = (ImageView) mainLinearLayout.findViewById(R.id.movieBackDrop);
            movie.setBackdrop(movie.getBitmapFromBytes(movie.getBackdropBytes()));
            if (movie.getPoster() == null) {
                String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/original" + movie.getBackdrop_path();
                new DownloadImageTask(backdrop).execute(path);
            } else {
                backdrop.setImageBitmap(movie.getBackdrop());
            }
        }

        if (movie.getPoster_path() != null) {
            ImageView poster = (ImageView) mainLinearLayout.findViewById(R.id.movie_posterview);
            movie.setPoster(movie.getBitmapFromBytes(movie.getPosterBytes()));
            if (movie.getPoster() == null) {
                String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/original" + movie.getPoster_path();
                new DownloadImageTask(poster).execute(path);
            } else {
                poster.setImageBitmap(movie.getPoster());
            }
        }
        TextView temp = (TextView) mainLinearLayout.findViewById(R.id.movieOriginalTitle);
        temp.setText(movie.getOriginal_title());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieGenre);
        temp.setText(movie.getGenres());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieStatus);
        temp.setText(movie.getStatus());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieReleaseDate);
        temp.setText(movie.getRelease_date());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieTagline);
        temp.setText(movie.getTagline());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieDescription);
        temp.setText(movie.getOverview());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieProductionCompanies);
        temp.setText(movie.getProduction_companies());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieProductionCountries);
        temp.setText(movie.getProduction_countries());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieBudget);
        temp.setText(String.valueOf(formatter.format(movie.getBudget())));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieRevenue);
        temp.setText(String.valueOf(formatter.format(movie.getRevenue())));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieRuntime);
        temp.setText(String.valueOf(movie.getRuntime()) + " Minutes");

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieHomepage);
        temp.setText(movie.getHomepage());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieVoteCount);
        temp.setText(String.valueOf(movie.getVote_count()));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieVoteAverage);
        temp.setText(String.valueOf(movie.getVote_average()));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieTitle);
        temp.setText(movie.getTitle());

        mainLinearLayout.invalidate();

    }

}