package com.rytan.moviecollection;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.db4o.ObjectSet;
import com.googlecode.androidannotations.annotations.*;
import com.rytan.moviecollection.Database.DatabaseHelper;
import com.rytan.moviecollection.ImageCache;
import com.rytan.moviecollection.tmdb.TMDb;
import com.rytan.moviecollection.tmdb.collections.Cast;
import com.rytan.moviecollection.tmdb.collections.Crew;
import com.rytan.moviecollection.tmdb.collections.MovieCast;
import com.rytan.moviecollection.tmdb.movie.Genre;
import com.rytan.moviecollection.tmdb.movie.Movie;
import com.rytan.moviecollection.tmdb.movie.ProductionCompany;
import com.rytan.moviecollection.tmdb.movie.ProductionCountry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.os.Environment.isExternalStorageRemovable;
import static com.rytan.moviecollection.ImageCache.getExternalCacheDir;

/**
 * User: Nate
 * Date: 5/13/13
 */
@EFragment(R.layout.detail_fragment)
public class SearchDetailFragment extends Fragment {
    TMDb tmdb = new TMDb(Constants.apiKey);

    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    ProgressDialog pDialog;

    ArrayList<Movie> databaseResult;
    Movie movie;


    int movieID = 11;
    NumberFormat formatter = NumberFormat.getCurrencyInstance();

    private DatabaseHelper dbHelper = null;
    String database;

    @ViewById(R.id.button2)
    TextView mWishlist;

    @ViewById(R.id.mainDetailsLinearLayout)
    LinearLayout mainLinearLayout;

    private static Bitmap temp;

    public static final SearchDetailFragment newInstance(String message) {

        SearchDetailFragment_ f = new SearchDetailFragment_();

        Bundle bdl = new Bundle(1);
        bdl.putString(EXTRA_MESSAGE, message);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //TODO: get data from intent in stead of static variable
        Bundle extras = getActivity().getIntent().getExtras();
        movieID = extras.getInt("movie");
        mImage = AsyncImage.getInstance(getActivity());

        pDialog = ProgressDialog.show(getActivity(), "Retrieving Data", "please wait...", true, false);

        LoadMovie loadMovie = new LoadMovie();
        loadMovie.execute();
        View view = inflater.inflate(R.layout.detail_fragment,
                container, false);

        return view;
    }


    @Override
    public void onPause() {
        super.onDestroy();
        dbHelper().close();
        dbHelper = null;
    }

    private DatabaseHelper dbHelper() {
        if (dbHelper == null) {
            dbHelper = new DatabaseHelper(getActivity());
            dbHelper.db(database);
        }
        return dbHelper;
    }

    @AfterViews
    void After() {
           initDiskCache();
    }

    @Background
    void initDiskCache(){
        mMemoryCache.initDiskCache();
    }

    @Click(R.id.move_to_collection)
    void BtnMoveToCollection() {
        Movie search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null,null,null,null,null);
        databaseResult = new ArrayList<Movie>();


        database = getResources().getString(R.string.collection_database);
        dbHelper();
        try {
            ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);
            while (result.hasNext()) {
                databaseResult.add(result.next());
            }


            if (databaseResult == null || databaseResult.size() < 1) {
                dbHelper.db(database).store(movie);
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie added to Collection", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie already in Collection", Toast.LENGTH_LONG).show();
            }

        } finally {
            dbHelper.db(database).close();
        }
        databaseResult = null;
    }

    @Click(R.id.add_to_wishlist)
    void BtnWishlist() {
        Movie search = new Movie(null, null, null, null, movieID, null, null, null, null, null, null
                , null, null, null, null, null, null, null, null, null, null,null,null,null,null);
        database = getResources().getString(R.string.wishlist_database);
        dbHelper();
        databaseResult = new ArrayList<Movie>();
        try {
            ObjectSet<Movie> result = dbHelper.db(database).queryByExample(search);
            while (result.hasNext()) {
                databaseResult.add(result.next());
            }


            if (databaseResult.size() < 1) {
                dbHelper.db(database).store(movie);
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie added to Wishlist", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(),
                        "Movie already in Wishlist", Toast.LENGTH_LONG).show();
            }

        } finally {
            dbHelper.db(database).close();
        }
        databaseResult = null;
    }


    public Bitmap getBitmapFromMemCache(String key) {
        BitmapDrawable ret = mMemoryCache.getBitmapFromMemCache(key);
        if( ret != null){
            return ret.getBitmap();
        }
        return mMemoryCache.getBitmapFromDiskCache(key);
    }

    private class LoadMovie extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            pDialog.show();
        }


        @Override
        protected Void doInBackground(Void... params) {

            String result = "";
            final Bitmap[] temp = new Bitmap[2];

            try {
                result = tmdb.SearchMovieById(String.valueOf(movieID));
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            try {
                JSONObject film = new JSONObject(result);
                Movie example = new Movie();
                example.setAdult(false);
                example.setBackdrop_path(film.getString("backdrop_path"));
                example.setId(Integer.parseInt(film.getString("id")));
                example.setOriginal_title(film.getString("original_title"));
                example.setRelease_date(film.getString("release_date"));
                example.setPoster_path(film.getString("poster_path"));
                example.setGenres(getGenreAsList(film.getJSONArray("genres")));
                example.setPopularity(Float.parseFloat(film.getString("popularity")));
                example.setTitle(film.getString("title"));
                example.setProduction_companies(getProductionCompaniesAsList(film.getJSONArray("production_companies")));
                example.setProduction_countries(getProductionCountriesAsList(film.getJSONArray("production_countries")));
                example.setVote_average(Float.parseFloat(film.getString("vote_average")));
                example.setVote_count(Integer.parseInt(film.getString("vote_count")));
                //example.setBelongs_to_collection(film.get("belongs_to_collection"));
                example.setHomepage(film.getString("homepage"));
                example.setBudget(Long.parseLong(film.getString("budget")));
                example.setImdb_id(film.getString("imdb_id"));
                example.setOverview(film.getString("overview"));
                example.setRevenue(Integer.parseInt(film.getString("revenue")));
                example.setRuntime(Integer.parseInt(film.getString("runtime")));
                example.setStatus(film.getString("status"));
                example.setTagline(film.getString("tagline"));
                example.setCast(retreveCast((Integer) example.getId()));
                if (example.getBackdrop_path() != null) {
                    String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w154" + example.getBackdrop_path();
                    DownloadBackdropTask task = new DownloadBackdropTask();
                    task.execute(path);
                }
                if (example.getPoster_path() != null) {
                    String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/original" + example.getPoster_path();
                    DownloadPosterTask task = new DownloadPosterTask();
                    task.execute(path);
                }
                movie = example;
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            PostExecute();
        }
    }

    private MovieCast retreveCast(int id) {
        movieID = id;
        MovieCast cast = new MovieCast();
        ArrayList mCast = new ArrayList<Cast>();
        String result = "";
        try {
            result = tmdb.GetMovieCast(Integer.toString(movieID));

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        JSONObject tempObj;
        JSONArray tempArray;

        try {
            tempObj = new JSONObject(result);
            tempArray = tempObj.getJSONArray("cast");
            for (int i = 0; i < tempArray.length(); i++) {

                JSONObject actor = tempArray.getJSONObject(i);
                Cast example = new Cast();
                example.setId(Integer.valueOf(actor.getString("id")));
                example.setName(actor.getString("name"));
                example.setCharacter(actor.getString("character"));
                example.setOrder(Integer.parseInt(actor.getString("order")));
                example.setProfile_path(actor.getString("profile_path"));
                 if (example.getProfile_path() != null) {
                    String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w92" + example.getProfile_path();
                    DownloadCastImageTask task = new DownloadCastImageTask();
                    task.execute(path);
                }
                mCast.add(example);
                temp =null;
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        Collections.sort(mCast, new Comparator<Cast>() {
            @Override
            public int compare(Cast left, Cast right) {
                int leftInt = (Integer) left.order;
                int rightInt = (Integer) right.order;
                return leftInt - rightInt;
            }
        });
        cast.setCast(mCast);

        ArrayList mCrew = new ArrayList<Crew>();
        result = "";
        try {
            result = tmdb.GetMovieCast(Integer.toString(movieID));

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        tempObj = null;
        tempArray = null;

        try {
            tempObj = new JSONObject(result);
            tempArray = tempObj.getJSONArray("crew");
            for (int i = 0; i < tempArray.length(); i++) {
                JSONObject actor = tempArray.getJSONObject(i);
                Crew example = new Crew();
                example.setId(Integer.valueOf(actor.getString("id")));
                example.setName(actor.getString("name"));
                example.setJob(actor.getString("job"));
                example.setDepartment(actor.getString("department"));
                example.setProfile_path(actor.getString("profile_path"));
                if (example.getProfile_path() != null) {
                    String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/w92" + example.getProfile_path();
                    DownloadCastImageTask task = new DownloadCastImageTask();
                    task.execute(path);
                }
                mCrew.add(example);
                temp = null;
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        cast.setCrew(mCrew);

        return cast;
    }
    private Bitmap getImage(String url){



        return temp;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadImageTask() {
            this.bmImage = new ImageView(getActivity());
        }

        protected void onPreExecute() {

        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {

                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(),mIcon11);

            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            try {
                bmImage.setImageBitmap(result);
                //close

            } catch (Exception ex) {
                Log.e("ERROR", "can't assign image to view");
            }

        }
    }
    private class DownloadCastImageTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView bmImage;

        public DownloadCastImageTask() {
        }

        protected void onPreExecute() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(),mIcon11);

            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            try {
                bmImage.setImageBitmap(result);
                //close

            } catch (Exception ex) {
                Log.e("ERROR", "can't assign image to view");
            }

        }
    }

    private class DownloadPosterTask extends AsyncTask<String, Void, Bitmap> {

        protected void onPreExecute() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {

                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(),mIcon11);

            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            try {
                movie.setPoster(result);
                movie.setPosterBytes(movie.changeImagetoBytes(movie.getPoster()));
                //close

            } catch (Exception ex) {
                Log.e("ERROR", "can't assign image to view");
            }

        }
    }

    private class DownloadBackdropTask extends AsyncTask<String, Void, Bitmap> {

        protected void onPreExecute() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {

                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(),mIcon11);

            } catch (Exception e) {
                Log.e("Error", "image download error");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            //set image of your imageview
            try {
                movie.setBackdrop(result);
                movie.setBackdropBytes(movie.changeImagetoBytes(movie.getBackdrop()));
                //close

            } catch (Exception ex) {
                Log.e("ERROR", "can't assign image to view");
            }

        }
    }


    private List<Genre> getGenreAsList(JSONArray genres) {
        ArrayList<Genre> listGenres = new ArrayList<Genre>();
        if (genres != null) {
            for (int x = 0; x < genres.length(); x++) {
                try {
                    JSONObject temp = genres.getJSONObject(x);
                    Genre genre = new Genre();
                    genre.setId(Integer.parseInt(temp.getString("id")));
                    genre.setName(temp.getString("name"));
                    listGenres.add(genre);
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return listGenres;
    }

    private List<ProductionCompany> getProductionCompaniesAsList(JSONArray genres) {
        ArrayList<ProductionCompany> listCompanies = new ArrayList<ProductionCompany>();
        if (genres != null) {
            for (int x = 0; x < genres.length(); x++) {
                try {
                    JSONObject temp = genres.getJSONObject(x);
                    ProductionCompany company = new ProductionCompany();
                    company.setId(Integer.parseInt(temp.getString("id")));
                    company.setName(temp.getString("name"));
                    listCompanies.add(company);
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return listCompanies;
    }

    private List<ProductionCountry> getProductionCountriesAsList(JSONArray genres) {
        ArrayList<ProductionCountry> listCompanies = new ArrayList<ProductionCountry>();
        if (genres != null) {
            for (int x = 0; x < genres.length(); x++) {
                try {
                    JSONObject temp = genres.getJSONObject(x);
                    ProductionCountry country = new ProductionCountry();
                    country.setIso_3166_1(temp.getString("iso_3166_1"));
                    country.setName(temp.getString("name"));
                    listCompanies.add(country);
                } catch (JSONException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return listCompanies;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @UiThread
    void PostExecute() {
        //TODO: Finish adding attributes to this view(WHERE THE HELL SHOULD THE CODE BE PUT?)
        if (movie.getBackdrop() != null) {
            ImageView backdrop = (ImageView) mainLinearLayout.findViewById(R.id.movieBackDrop);
            if (movie.getPoster() == null) {
                String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/original" + movie.getBackdrop_path();
                new DownloadImageTask(backdrop).execute(path);
            } else {
                backdrop.setImageBitmap(movie.getBackdrop());
            }
        }

        if (movie.getPoster() != null) {
            ImageView poster = (ImageView) mainLinearLayout.findViewById(R.id.movie_posterview);
            if (movie.getPoster() == null) {
                String path = "http://d3gtl9l2a4fn1j.cloudfront.net/t/p/original" + movie.getPoster_path();
                new DownloadImageTask(poster).execute(path);
            } else {
                poster.setImageBitmap(movie.getPoster());
            }
        }

        TextView temp = (TextView) mainLinearLayout.findViewById(R.id.movieOriginalTitle);
        temp.setText(movie.getOriginal_title());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieGenre);
        temp.setText(movie.getGenres());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieStatus);
        temp.setText(movie.getStatus());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieReleaseDate);
        temp.setText(movie.getRelease_date());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieTagline);
        temp.setText(movie.getTagline());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieDescription);
        temp.setText(movie.getOverview());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieProductionCompanies);
        temp.setText(movie.getProduction_companies());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieProductionCountries);
        temp.setText(movie.getProduction_countries());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieBudget);
        temp.setText(String.valueOf(formatter.format(movie.getBudget())));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieRevenue);
        temp.setText(String.valueOf(formatter.format(movie.getRevenue())));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieRuntime);
        temp.setText(String.valueOf(movie.getRuntime()) + " Minutes");

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieHomepage);
        temp.setText(movie.getHomepage());

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieVoteCount);
        temp.setText(String.valueOf(movie.getVote_count()));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieVoteAverage);
        temp.setText(String.valueOf(movie.getVote_average()));

        temp = (TextView) mainLinearLayout.findViewById(R.id.movieTitle);
        temp.setText(movie.getTitle());

        mainLinearLayout.invalidate();
        pDialog.dismiss();

    }

}